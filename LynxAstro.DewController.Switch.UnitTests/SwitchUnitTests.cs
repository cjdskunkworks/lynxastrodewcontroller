﻿using System.Collections.Generic;
using ASCOM.LynxAstro.DewController;
using Moq;
using NUnit.Framework;

namespace LynxAstro.DewController.Switch.UnitTests
{
    [TestFixture]
    public class SwitchUnitTests
    {
        private ASCOM.LynxAstro.DewController.Switch _switch;
        private Mock<ISharedResourcesWrapper> _sharedResourcesWrapperMock;

        private ProfileProperties _profileProperties;

        [SetUp]
        public void Setup()
        {
            _profileProperties = new ProfileProperties()
            {
                ComPort = "TestCom1",
                SwitchNames = new List<string>(),
                TraceLogger = false
            };

            _sharedResourcesWrapperMock = new Mock<ISharedResourcesWrapper>();
            _sharedResourcesWrapperMock.Setup(x => x.ReadProfile()).Returns(() => _profileProperties);

            _switch = new ASCOM.LynxAstro.DewController.Switch(_sharedResourcesWrapperMock.Object);
        }

        [Test]
        public void CheckThatClassCreatedProperly()
        {
            Assert.That(_switch, Is.Not.Null);
        }
    }
}
