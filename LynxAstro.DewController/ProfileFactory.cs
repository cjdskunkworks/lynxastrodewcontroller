namespace ASCOM.LynxAstro.DewController
{
    public class ProfileFactory : IProfileFactory
    {
        public IProfileWrapper Create()
        {
            return new ProfileWrapper();
        }
    }
}