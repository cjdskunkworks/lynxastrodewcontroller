using System.Collections.Generic;

namespace ASCOM.LynxAstro.DewController
{
    public class ProfileProperties
    {
        // properies that are part of the profile
        public string ComPort { get; set; }
        public bool TraceLogger { get; set; }
        public List<string> SwitchNames { get; set; } = new List<string>();
    }
}