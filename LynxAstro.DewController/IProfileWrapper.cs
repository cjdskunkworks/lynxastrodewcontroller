using System;
using ASCOM.Utilities.Interfaces;

namespace ASCOM.LynxAstro.DewController
{
    public interface IProfileWrapper : IProfile, IProfileExtra, IDisposable
    {

    }
}