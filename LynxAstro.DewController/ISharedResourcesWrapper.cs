using System;
using ASCOM.Utilities.Interfaces;

namespace ASCOM.LynxAstro.DewController
{
    public interface ISharedResourcesWrapper
    {
        ConnectionInfo Connect(string deviceId, string driverId, ITraceLogger traceLogger);
        void Disconnect(string deviceId, string driverId);
        
        string FirmwareVersion { get; }

        void Lock(Action action);
        T Lock<T>(Func<T> func);

        string SendString(string message);
        void SendBlind(string message);
        string SendChar(string message);

        string ReadTerminated();

        ProfileProperties ReadProfile();

        void SetupDialog();
        void WriteProfile(ProfileProperties profileProperties);
        void ReadCharacters(int throwAwayCharacters);
    }
}