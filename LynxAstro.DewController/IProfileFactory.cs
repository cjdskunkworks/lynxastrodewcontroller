namespace ASCOM.LynxAstro.DewController
{
    public interface IProfileFactory
    {
        IProfileWrapper Create();
    }
}