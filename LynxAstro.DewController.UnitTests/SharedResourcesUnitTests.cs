﻿using System;
using ASCOM.LynxAstro.DewController;
using ASCOM.Utilities.Interfaces;
using Moq;
using NUnit.Framework;

namespace LynxAstro.DewController.UnitTests
{
    [TestFixture]
    public class SharedResourcesUnitTests
    {
        private Mock<ISerial> _serialMock;
        private Mock<ITraceLogger> _traceLoggerMock;

        [SetUp]
        public void Setup()
        {
            _serialMock = new Mock<ISerial>();
            _serialMock.SetupAllProperties();

            _traceLoggerMock = new Mock<ITraceLogger>();

            SharedResources.SharedSerial = _serialMock.Object;
        }

        [Test]
        public void CheckThatSerialPortIsSetToUseMock()
        {
            Assert.That(SharedResources.SharedSerial, Is.EqualTo(_serialMock.Object));
        }

        [Test]
        public void SendBlind_WhenCalled_Then_ClearsBuffersAndSendsMessage()
        {
            var expectedMessage = "Test";

            SharedResources.SendBlind(expectedMessage);

            _serialMock.Verify(x => x.ClearBuffers(), Times.Once);
            _serialMock.Verify(x => x.Transmit(expectedMessage), Times.Once);
        }

        [Test]
        public void SendChar_WhenCalled_ThenSendsMessageAndReadsExpectedNumberOfCharacters()
        {
            var expectedMessage = "Test";
            var expectedResult = "A";

            _serialMock.Setup(x => x.ReceiveCounted(1)).Returns(expectedResult);

            var result = SharedResources.SendChar(expectedMessage);

            _serialMock.Verify(x => x.ClearBuffers(), Times.Once);
            _serialMock.Verify(x => x.Transmit(expectedMessage), Times.Once);
            _serialMock.Verify(x => x.ReceiveCounted(1), Times.Once);
            Assert.That(result, Is.EqualTo(expectedResult));
        }

        [Test]
        public void SendString_WhenCalled_ThenSendsMessageAndReadsResultUntilTerminatorFound()
        {
            var expectedMessage = "Test";
            var expectedResult = "TestMessage#";

            _serialMock.Setup(x => x.ReceiveTerminated("#")).Returns(expectedResult);

            var result = SharedResources.SendString(expectedMessage);

            _serialMock.Verify(x => x.ClearBuffers(), Times.Once);
            _serialMock.Verify(x => x.Transmit(expectedMessage), Times.Once);
            _serialMock.Verify(x => x.ReceiveTerminated("#"), Times.Once);
            Assert.That(result, Is.EqualTo(expectedResult.TrimEnd('#')));
        }

        [Test]
        public void ReadTerminated_WhenCalled_ThenReadsResultUntilTerminatorFound()
        {
            var expectedResult = "TestMessage#";

            _serialMock.Setup(x => x.ReceiveTerminated("#")).Returns(expectedResult);

            var result = SharedResources.ReadTerminated();

            _serialMock.Verify(x => x.ReceiveTerminated("#"), Times.Once);
            Assert.That(result, Is.EqualTo(expectedResult));
        }

        [Test]
        public void ReadCharacters_WhenCalled_ThenReadsSpecificNumberOfCharacters()
        {
            var numberOfCharacters = 5;

            SharedResources.ReadCharacters(numberOfCharacters);

            _serialMock.Verify(x => x.ReceiveCounted(numberOfCharacters), Times.Once);
        }

        [Test]
        public void WriteProfile_WhenCalled_WritesExpectedProfileSettings()
        {
            string DriverId = "ASCOM.LynxAstro.DewController.Switch";

            Mock<IProfileWrapper> profileWrapperMock = new Mock<IProfileWrapper>();
            profileWrapperMock.SetupAllProperties();

            IProfileWrapper profeWrapper = profileWrapperMock.Object;

            Mock<IProfileFactory> profileFactoryMock = new Mock<IProfileFactory>();
            profileFactoryMock.Setup(x => x.Create()).Returns(profileWrapperMock.Object);

            SharedResources.ProfileFactory = profileFactoryMock.Object;

            var profileProperties = new ProfileProperties
            {
                TraceLogger = false,
                ComPort = "TestComPort"
            };

            SharedResources.WriteProfile(profileProperties);

            Assert.That(profeWrapper.DeviceType, Is.EqualTo("Switch"));
            profileWrapperMock.Verify(x => x.WriteValue(DriverId, "Trace Level", profileProperties.TraceLogger.ToString()), Times.Once);
            profileWrapperMock.Verify(x => x.WriteValue(DriverId, "COM Port", profileProperties.ComPort), Times.Once);
        }

        [Test]
        public void ReadProfile_WhenCalled_ReturnsExpectedDefaultValues()
        {
            string DriverId = "ASCOM.LynxAstro.DewController.Switch";

            string ComPortDefault = "COM1";
            string TraceStateDefault = "false";
            
            Mock<IProfileWrapper> profileWrapperMock = new Mock<IProfileWrapper>();
            profileWrapperMock.SetupAllProperties();

            profileWrapperMock.Setup(x => x.DeviceType).Returns("Switch");

            profileWrapperMock.Setup(x => x.GetValue(DriverId, "Trace Level", string.Empty, TraceStateDefault))
                .Returns(() =>
                    TraceStateDefault);
            profileWrapperMock.Setup(x => x.GetValue(DriverId, "COM Port", string.Empty, ComPortDefault))
                .Returns(ComPortDefault);
            
            profileWrapperMock.Setup(x =>
                    x.GetValue(DriverId, It.IsRegex("^SwitchName_[0-9{1}]$"), string.Empty, It.IsAny<string>()))
                .Returns((string driverId, string name, string subKey, string defaultValue) => defaultValue);

            IProfileWrapper profeWrapper = profileWrapperMock.Object;

            Mock<IProfileFactory> profileFactoryMock = new Mock<IProfileFactory>();
            profileFactoryMock.Setup(x => x.Create()).Returns(profileWrapperMock.Object);

            SharedResources.ProfileFactory = profileFactoryMock.Object;

            var profileProperties = SharedResources.ReadProfile();

            Assert.That(profeWrapper.DeviceType, Is.EqualTo("Switch"));
            Assert.That(profileProperties.ComPort, Is.EqualTo(ComPortDefault));
            Assert.That(profileProperties.TraceLogger, Is.EqualTo(bool.Parse(TraceStateDefault)));
        }

        [TestCase("TCP")]
        [TestCase("Carrier Pigeon")]
        public void Connect_WhenDeviceIdIsNotSerial_ThenThrowsException(string deviceId)
        {
            var result = Assert.Throws<ArgumentException>(() => { SharedResources.Connect(deviceId, string.Empty, _traceLoggerMock.Object); });

            Assert.That(result.Message, Is.EqualTo($"deviceId {deviceId} not currently supported"));
        }
    }
}
