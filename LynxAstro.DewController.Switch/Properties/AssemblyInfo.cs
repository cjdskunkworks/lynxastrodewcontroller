﻿using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
//
// TODO - Add your authorship information here
[assembly: AssemblyTitle("ASCOM.LynxAstro.DewController.Switch")]
[assembly: AssemblyDescription("ASCOM Switch driver for LynxAstro.DewController")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("The ASCOM Initiative")]
[assembly: AssemblyProduct("ASCOM Switch driver for LynxAstro.DewController")]
[assembly: AssemblyCopyright("Copyright © 2021 The ASCOM Initiative")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(true)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("a567b01c-a066-45ce-af3d-0192bad973ea")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
//
// TODO - Set your driver's version here
[assembly: AssemblyVersion("6.5.1.0")]
[assembly: AssemblyFileVersion("6.5.1.0")]
